<?php
namespace ShadowCMS\Objects;

class ChangeLog extends \ShadowCMS\BaseObject
{

	public static $strObjectName = "change_log";
	public static $strIndexProp = "change_log_id";
	public static $strTableName = "change_logs";

	public static $arrProps = array(
		"change_log_id",
		"change_log_time",
		"change_log_ip",
		"change_log_ref_name",
		"change_log_ref_id",
		"change_log_type",
		"user_id"
	);


	public static $arrPropsEnum = array(
		"change_log_type" => [
			"add",
			"edit",
			"delete"
		]
	);

	public static $arrPropsOptional = array(
		"user_id" => NULL
	);

	public static $arrPropsReadOnly = array(
		"change_log_time"
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();

}