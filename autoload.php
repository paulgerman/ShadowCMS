<?php
namespace ShadowCMS;

use Exception;
const LIBRARY_NAME = "ShadowCMS";
require_once("functions.php");

spl_autoload_register(
	function($strClassName)
	{
		if (strpos($strClassName, LIBRARY_NAME) === 0)
		{
			$strFileName = __DIR__.DIRECTORY_SEPARATOR.str_replace("\\", DIRECTORY_SEPARATOR, substr($strClassName, strlen(LIBRARY_NAME) +1)).".php";
			if(!file_exists($strFileName))
				throw new Exception("File does not exist ". $strFileName);
			require_once($strFileName);
		}
	}
);