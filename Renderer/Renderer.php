<?php
/**
 * THIS IS LEGACY CODE - here only for backwards compatibility
 */
namespace ShadowCMS\Renderer;
use ShadowCMS\GeneralException;

class Renderer
{
	const ACTION_ADD = 1;
	const ACTION_EDIT = 2;
	const ACTION_DELETE = 3;
	const ACTION_REORDER = 4;
	const ACTION_NONE = 5;

	private $arrObject;
	private $strObjectName;
	private $strFormAttributes = "";
	private $arrCustomOptions = array();
	private $strIDsPrefix;

	function __construct($strObjectName, $strIDsPrefix = "")
	{
		$this->strObjectName = $strObjectName;
		$this->strIDsPrefix = $strIDsPrefix;
		$this->arrObject = null;
	}

	function setObject(array $arrObject)
	{
		$this->arrObject = $arrObject;
	}

	function addCustomOptions($strKey, $arrData)
	{
		$this->arrCustomOptions[$strKey] = $arrData;
	}

	function setCustomFormAttributes($strAttributes)
	{
		$this->strFormAttributes = $strAttributes;
	}

	public function renderTable($arrObjects, $arrActions = array(), $arrHiddenKeys = array(), $arrRenderOptions = array())
	{
		$strObjectName = $this->strObjectName;

		$strHTML = '<table class="table table-hover table-striped" id="'.$this->strIDsPrefix.'table">
						<thead>
							<tr>';


		foreach($strObjectName::$arrRenderProps as $strKey => $arrOptions)
		{
			if(!is_string($arrOptions) && !in_array($strKey, $arrHiddenKeys))
				$strHTML .= "<th>".$arrOptions["displayName"]."</th>";
		}
		if(count($arrActions))
		{
			$strHTML .= "<th>---</th>";
		}

		$strHTML .= "</tr>
		    </thead>
		<tbody>";

		foreach($arrObjects as $object)
		{
			$strHTML .= "<tr>";
			foreach($strObjectName::$arrRenderProps as $strKey => $arrOptions)
			{
				if(is_string($arrOptions) || in_array($strKey, $arrHiddenKeys))
					continue;

				$strHTML .= "<td>";
				if(array_key_exists("internalType", $arrOptions))
				{
					$strDisplayFunction = $arrOptions["internalType"]."ElementDisplay";
					if(!method_exists($this, $strDisplayFunction))
					{
						throw new GeneralException("Function ".$strDisplayFunction." does not exist in class ".get_class($this));
					}
					$strHTML .= $this->$strDisplayFunction($object, $strKey);
				}
				else
				{
					$nSizeLimit = 999999;
					if(array_key_exists("maxDisplaySize", $arrOptions))
						$nSizeLimit = $arrOptions["maxDisplaySize"];

					if(array_key_exists("displayOptions", $arrOptions))
					{
						$arrDisplayOptions = array_combine($strObjectName::$arrPropsEnum[$strKey], $arrOptions["displayOptions"]);

						$strHTML .= mb_strimwidth($arrDisplayOptions[$object[$strKey]], 0, $nSizeLimit, "...");
					}
					else if(is_array($object[$strKey]))
					{
						$strHTML .= mb_strimwidth(implode(", ", $object[$strKey]), 0, $nSizeLimit, "...");
					}
					else
					{
						$strHTML .= nl2br(htmlspecialchars(mb_strimwidth($object[$strKey], 0, $nSizeLimit, "...")));
					}
				}
				$strHTML .= "</td>";
			}
			if(count($arrActions))
			{
				$strHTML .= "<td>";
				foreach($arrActions as $arrAction)
				{
					if(array_key_exists("link", $arrAction))
						$strHTML .= '<a href="'.$arrAction["link"].$object[$strObjectName::$strIndexProp].'">';

					$strHTML .= '<button type="button"';
					if(array_key_exists("method", $arrAction))
						$strHTML .= 'data-method="'.$arrAction["method"].'" data-id="'.$object[$strObjectName::$strIndexProp].'"';
					$strHTML .= ' class="btn btn-primary btn-sm ';

					if(array_key_exists("class", $arrAction))
						$strHTML .= $arrAction["class"];

					$strHTML .= '" style="margin-bottom:2px;">'.$arrAction["text"].'</button>';

					if(array_key_exists("link", $arrAction))
						$strHTML .= '</a>';

					$strHTML .= "<br>";
				}
				$strHTML .= "</td>";
			}
			$strHTML .= "</tr>";
		}

		$arrRenderOptions = array_merge_recursive(
			$arrRenderOptions,
			array(
				"stateSave" => true,
				"stateDuration" => 60 * 60 * 24 * 31,
				"dom" => "Bfrltip",
				"buttons" => array(
					'colvis'
				),
				"colReorder" => true,
				"fixedHeader" => true,
			)
		);

		$strHTML .= "</tbody>
  			</table>
  			<script>
				$(document).ready(function(){
				    var table".$this->strIDsPrefix." = $('#".$this->strIDsPrefix."table').DataTable(
				        JSON.parse('".json_encode($arrRenderOptions)."')
				    );
				    $('#".$this->strIDsPrefix."table tbody').on( 'click', '.delete', function () {
				        if(confirm('Are you sure you want to delete this item?'))
				        {
					        var self = this;
						    call(
						        $(this).attr('data-method'),
						        [$(this).attr('data-id')], 
						        function (){
							        $(self).parents('tr').fadeOut(
							            'fast',
							            'swing',
								        function (){
											table".$this->strIDsPrefix."
										        .row( $(self).parents('tr') )
										        .remove()
										        .draw();
									    }
									)
								}
						    );
						}
					});
					$('#".$this->strIDsPrefix."table').css('width','100%');

				    $('a[data-toggle=\"tab\"]').on( 'shown.bs.tab', function (e) {
				        var target = $(e.target).attr(\"href\") // activated tab
						var activeTab = $(target + 'table');
				        $.fn.dataTable.tables( {visible: false, api: true} ).columns.adjust();
				        activeTab.DataTable.tables( {visible: true, api: true} ).columns.adjust();
				        $('.select2-search__field').width('100%'); 
				    });
				    
		
				});
			</script>";

		return $strHTML;
	}

	public function renderForm($arrHiddenProps = array(), $bAllowAdd = true)
	{
		$this->formID = uniqid();
		$strObjectName = $this->strObjectName;
		$arrObject = $this->arrObject;
		if($bAllowAdd === false && $arrObject == null)
		{
			$strHTML = '<h2>Select one element to edit</h2>';

			return $strHTML;
		}


		$strHTML = '<form method="post" enctype="multipart/form-data" role="form" '.$this->strFormAttributes.'>';
		if($this->arrObject != null)
		{
			$strHTML .= '<input type="hidden" name="edit" value="1">';
		}
		else
		{
			$strHTML .= '<input type="hidden" name="create" value="1">';
		}

		foreach($arrHiddenProps as $strName => $strValue)
		{
			$strHTML .= '<input type="hidden" name="'.$strName.'" value="'.$strValue.'">';
		}

		foreach($strObjectName::$arrRenderProps as $strKey => $arrOptions)
		{
			if(
				!array_key_exists($strKey, $strObjectName::$arrRenderProps)
				|| array_key_exists($strKey, $arrHiddenProps)
			)
			{
				continue;
			}

			if(is_string($arrOptions))
			{
				$strHTML .= $arrOptions;
				continue;
			}

			if($strKey == $strObjectName::$strIndexProp && $arrObject != null || $strKey != $strObjectName::$strIndexProp)
			{
				$strHTML .= '<div class="form-group">';

				$strDisplayName = $strKey;
				if(array_key_exists("displayName", $arrOptions))
					$strDisplayName = $arrOptions["displayName"];

				$strHTML .= '<label for="'.$strKey.'">'.$strDisplayName.'</label>';

				if(array_key_exists("tooltip", $arrOptions))
				{
					$strHTML .= " ".$arrOptions["tooltip"];
				}
				$strHTML .= ':';

				if(array_key_exists("internalType", $arrOptions))
				{
					$strDisplayFunction = strtolower($arrOptions["internalType"])."ElementDisplay";
					$strEditFunction = strtolower($arrOptions["internalType"])."ElementEdit";
					if(
						!method_exists($this, $strEditFunction)
						|| !method_exists($this, $strDisplayFunction)
					)
					{
						throw new GeneralException("Function ".$strEditFunction." or function ".$strDisplayFunction." does not exist in class ".get_class($this));
					}

					if($arrObject != null)
						$strHTML .= $this->$strDisplayFunction($arrObject, $strKey);

					$strHTML .= $this->$strEditFunction($strKey);
				}
				else
				{
					$strHTML .= $this->defaultElementEdit($strKey);
				}
				$strHTML .= '</div>';
			}

		}
		if($this->arrObject != null)
			$strButtonText = "Save";
		else
			$strButtonText = "Add";

		$strHTML .= '<input type="submit" class="btn btn-default" value="'.$strButtonText.'">';

		if($this->arrObject != null)
			$strHTML .= '<input style="margin-left: 20px;" name="reset" type="submit" class="btn btn-default" value="Close edit">';
		$strHTML .= '</form>';

		return $strHTML;
	}

	public function renderSortForm(array $arrObjects, $strSortKey, $strDisplayKey, $arrHiddenProps = array())
	{
		$nID = uniqid("sortable");
		if(!count($arrObjects))
		{
			$strHTML = "------------------";

			return $strHTML;
		}
		$strObjectName = $this->strObjectName;
		$strHTML = "";
		$strHTML = '<form method="post" role="form">';
		foreach($arrHiddenProps as $strKey => $strValue)
		{
			$strHTML .= '<input type="hidden" name="'.$strKey.'" value="'.$strValue.'">';
		}
		$strHTML .= '
						<input type="hidden" name="reorder" value="'.$strSortKey.'">
						<div id="'.$nID.'" class="list-group">';
		foreach($arrObjects as $object)
		{
			$strHTML .= '
				<div class="list-group-item move-item" style="cursor: hand;">
					<span class="glyphicon glyphicon-move" aria-hidden="true"></span>
					ID'.$object[$strObjectName::$strIndexProp].': '.$object[$strDisplayKey].'
					<input type="hidden" name="'.$strSortKey.'[]" value="'.$object[$strObjectName::$strIndexProp].'">
				</div>';

		}
		$strHTML .= '</div>';

		$strHTML .= '
			<script>
				// List with handle
				Sortable.create('.$nID.', {
				  handle: \'.move-item\',
				  animation: 150
				});
			</script>';

		$strHTML .= '<input type="submit" class="btn btn-default" value="Save">';
		$strHTML .= '</form>';

		return $strHTML;
	}

	public function doAction($arrValues)
	{
		$strAction = static::ACTION_NONE;
		$strObjectName = $this->strObjectName;
		if(count($arrValues))
		{
			$strControllerName = "\\Controllers\\".substr($strObjectName, strrpos($strObjectName, '\\') + 1)."Controller";

			error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".$strControllerName." ".print_r($arrValues, true)."\n\n",3,"logs/"."custom.log");
			/** @var \Controllers\BaseController $objController */
			$objController = new $strControllerName();

			if(array_key_exists("delete", $arrValues))
			{
				try
				{
					$objController->delete($arrValues["delete"]);
					$strAction = static::ACTION_DELETE;
				}
				catch(GeneralException $exc)
				{
					if($exc->getCode() != GeneralException::PRODUCT_NOT_FOUND)
					{
						throw $exc;
					}
				}
			}

			if(array_key_exists("reorder", $arrValues))
			{
				$strSortKey = $arrValues['reorder'];
				foreach($arrValues[$strSortKey] as $nOrder => $nObjectID)
				{
					$objController->edit($nObjectID, array(
						$strSortKey => $nOrder,
						$strObjectName::$strIndexProp => $nObjectID,
					));
				}
				$strAction = static::ACTION_REORDER;
			}
			else if(array_key_exists("edit", $arrValues))
			{
				$objController->edit($arrValues[$strObjectName::$strIndexProp], $arrValues);
				$strAction = static::ACTION_EDIT;
			}
			else if(array_key_exists("create", $arrValues))
			{
				$objController->create($arrValues);
				$strAction = static::ACTION_ADD;
			}
		}

		return $strAction;
	}

	function defaultElementEdit($strKey)
	{
		$arrObject = $this->arrObject;
		$strHTML = "";
		$strObjectName = $this->strObjectName;
		$arrOptions = array();
		if(array_key_exists($strKey, $strObjectName::$arrPropsEnum))
		{
			$strType = "select";
			if(array_key_exists("displayOptions", $strObjectName::$arrRenderProps[$strKey]))
			{
				//associate custom names from displayOptions with the default values that can be set
				foreach($strObjectName::$arrPropsEnum[$strKey] as $nKey => $nValue)
				{
					$arrOptions[] = array(
						"text" => $strObjectName::$arrRenderProps[$strKey]["displayOptions"][$nKey],
						"value" => $nValue
					);
				}
			}
			else
			{
				//set names as values if the custom displayOptions key does not exist
				foreach($strObjectName::$arrPropsEnum[$strKey] as $nKey => $nValue)
				{
					$arrOptions[] = array(
						"text" => $nValue,
						"value" => $nValue
					);
				}
			}
		}
		else
		{
			$strType = "text";
		}

		if(array_key_exists("type", $strObjectName::$arrRenderProps[$strKey]))
			$strType = $strObjectName::$arrRenderProps[$strKey]["type"];

		if($strType == "select")
		{
			if(empty($arrOptions))
			{
				if(!array_key_exists($strKey, $this->arrCustomOptions))
					throw new GeneralException("You haven't defined the select options for key ".$strKey);
				$arrOptions = $this->arrCustomOptions[$strKey];
			}

			$arrElementOptions = array(
				"class" => "form-control",
				"id" => $this->strIDsPrefix.$strKey,
				"name" => $strKey,
			);
			if(array_key_exists("internalAttributes", $strObjectName::$arrRenderProps[$strKey]))
			{
				$arrElementOptions = $strObjectName::$arrRenderProps[$strKey]["internalAttributes"] + $arrElementOptions;
			}

			$strElementContent = "";

			foreach($arrOptions as $arrItemOptions)
			{
				$strElementContent .= $this->recursiveSelectDisplay($arrItemOptions, $strKey);
			}
			$strHTML .= static::renderElement("select", $arrElementOptions, $strElementContent);
		}
		else if($strType == "textarea")
		{
			$strHTML .= '<textarea rows="5" class="form-control" id="'.$this->strIDsPrefix.$strKey.'" name="'.$strKey.'">';
			if($this->arrObject != null && array_key_exists($strKey, $arrObject))
			{
				$strHTML .= $arrObject[$strKey];
			}
			$strHTML .= '</textarea>';
		}
		else if($strType == "multiselect")
		{
			if(empty($arrOptions))
			{
				if(!array_key_exists($strKey, $this->arrCustomOptions))
					throw new GeneralException("You haven't defined the select options for key ".$strKey);
				$arrOptions = $this->arrCustomOptions[$strKey];
			}

			$arrElementOptions = array(
				"style" => "width:100%;",
				"class" => "multiselect form-control",
				"multiple" => "multiple",
				"id" => $this->strIDsPrefix.$strKey,
				"name" => $strKey."[]",
			);

			if(array_key_exists("internalAttributes", $strObjectName::$arrRenderProps[$strKey]))
			{
				$arrElementOptions = $strObjectName::$arrRenderProps[$strKey]["internalAttributes"] + $arrElementOptions;
			}

			$strElementContent = "";
			foreach($arrOptions as $arrItemOptions)
			{
				$strElementContent .= $this->recursiveSelectDisplay($arrItemOptions, $strKey);
			}

			$strHTML .= static::renderElement("select", $arrElementOptions, $strElementContent);

			$strHTML .= '
				<script type="text/javascript">
					$("#'.$this->strIDsPrefix.$strKey.'").select2(';
			if(array_key_exists("internalTypeOptions", $strObjectName::$arrRenderProps[$strKey]))
				$strHTML .= "JSON.parse('".json_encode($strObjectName::$arrRenderProps[$strKey]["internalTypeOptions"])."')";
			$strHTML .= '
					);
				</script>';
		}
		else
		{
			if(
				array_key_exists("internalAttributes", $strObjectName::$arrRenderProps[$strKey])
				&& array_key_exists("type", $strObjectName::$arrRenderProps[$strKey]["internalAttributes"])
			)
			{
				$strType = $strObjectName::$arrRenderProps[$strKey]["internalAttributes"]["type"];
			}

			if($strKey == $strObjectName::$strIndexProp)
			{
				$strHTML .= '<input type="hidden" name="'.$strKey.'" value="'.$arrObject[$strKey].'">';
				$strHTML .= '<input disabled="true" class="form-control" type="text" id="'.$this->strIDsPrefix.$strKey.'" ';
			}
			else
			{
				$strHTML .= '<input '.($strType !== "datetime-local"?'class="form-control"':'').' type="'.$strType.'" id="'.$this->strIDsPrefix.$strKey.'" name="'.$strKey.'" ';
			}



			if($this->arrObject != null && array_key_exists($strKey, $arrObject))
			{
				if($strType === "datetime-local")
				{
					$strHTML .= 'value="'.date("Y-m-d\TH:i", strtotime($arrObject[$strKey])).'" ';
				}
				else
				{
					$strHTML .= 'value="'.$arrObject[$strKey].'" ';
				}
			}
			$strHTML .= '>';
			if($strType == "file")
			{
				$strHTML .= "
					<script>
						checkFileSize('".$this->strIDsPrefix.$strKey."',".getMaximumFileUploadSize().");
					</script>";
			}
		}

		return $strHTML;
	}

	function recursiveSelectDisplay($arrItemOptions, $strKey)
	{
		$strHTML = "";

		if(array_key_exists("type", $arrItemOptions) && $arrItemOptions["type"] == "optgroup")
		{
			$strHTML .= '<optgroup label="'.$arrItemOptions["text"].'">';
			foreach($arrItemOptions["data"] as $arrSubItemOptions)
			{
				$strHTML .= $this->recursiveSelectDisplay($arrSubItemOptions, $strKey);
			}
			$strHTML .= '</optgroup>';
		}
		else
		{
			$strHTML .= '<option value="'.$arrItemOptions["value"].'"';
			if($this->arrObject != null
				&& (
					is_array($this->arrObject[$strKey]) && in_array($arrItemOptions["value"], $this->arrObject[$strKey])
					|| $this->arrObject[$strKey] == $arrItemOptions["value"]
				)
			)
			{
				$strHTML .= " selected";
			}
			if(array_key_exists("disabled", $arrItemOptions))
				$strHTML .= " disabled";
			$strHTML .= '>'.$arrItemOptions["text"].'</option>';
		}

		return $strHTML;
	}

	function imageElementEdit($strKey)
	{
		$strHTML = "";
		if($this->arrObject == null)
			$strHTML .= $this->imageElementDisplay(array($strKey => ""), $strKey);

		if($this->arrObject[$strKey] != "")
			$strHTML = '<a href="#" class="btn-danger btn-sm" title="Click to delete" onclick="remove_image(this, \''.$strKey.'\', \''.$this->strIDsPrefix.'\');return false;">X</a>';
		$strHTML .= $this->defaultElementEdit($strKey);
		$strHTML .= '
		<script>
			$( document ).ready(function() {
				$("#'.$this->strIDsPrefix.$strKey.'").change(function (){
					updateImage("'.$this->strIDsPrefix.$strKey.'");
				});
			});
        </script>';

		return $strHTML;
	}

	function imageElementDisplay($object, $strKey)
	{
		$strHTML = '<div style="background-color: grey; background-size: 20%; display: inline-block;"><img id="'.$this->strIDsPrefix.$strKey.'_show" src="'.$object[$strKey].'" style="max-width:160px; ';
		if($object[$strKey] == "")
			$strHTML .= "display:none;";

		$strHTML .= '"></div>';

		if($object[$strKey] == "" && $this->arrObject != null)
			$strHTML .= 'No image';

		return $strHTML;
	}

	function multiselectElementEdit($strKey)
	{
		return $this->defaultElementEdit($strKey);
	}

	function multiselectElementDisplay($object, $strKey)
	{
		$strObjectName = $this->strObjectName;
		//$strControllerName = "\\Controllers\\".substr($strObjectName, strrpos($strObjectName, '\\') + 1)."Controller";
		///** @var \Controllers\BaseController $objController */
		//$objController = new $strControllerName();

		$arrSelectableItems = array();
		foreach($this->arrCustomOptions[$strKey] as $arrItemOptions)
		{
			$arrSelectableItems += $this->_getSelectableItems($arrItemOptions);
		}

		$strHTML = "";
		foreach($object[$strKey] as $strSelectedKey)
		{
			$strHTML .= $arrSelectableItems[$strSelectedKey]["text"].", ";
		}
		$strHTML = rtrim($strHTML, ", ");

		return $strHTML;
	}


	/**
	 * @param $arrItemOptions
	 * @return array
	 */
	private function _getSelectableItems($arrItemOptions)
	{
		$arrItems = array();
		if(array_key_exists("type", $arrItemOptions) && $arrItemOptions["type"] == "optgroup")
		{
			foreach($arrItemOptions["data"] as $arrSubItemOptions)
			{
				$arrItems += $this->_getSelectableItems($arrSubItemOptions);
			}
		}
		else
		{
			$arrItems[$arrItemOptions["value"]] = $arrItemOptions;
		}

		return $arrItems;
	}

	function fileElementEdit($strKey)
	{
		return $this->defaultElementEdit($strKey);
	}

	function fileElementDisplay($object, $strKey)
	{
		$strHTML = '<a target="_blank" href="http://'.$_SERVER['SERVER_NAME'].$object[$strKey].'">'.'http://'.$_SERVER['SERVER_NAME'].$object[$strKey].'</a>';

		return $strHTML;
	}

	function colorRowElementEdit($strKey)
	{
		return $this->defaultElementEdit($strKey);
	}

	function colorRowElementDisplay($object, $strKey)
	{
		if($object[$strKey] == "red")
			$color = '#FF9999';
		else if($object[$strKey] == "yellow")
			$color = '#ffffb3';
		else if($object[$strKey] == "orange")
			$color = '#ffcc99';
		else if($object[$strKey] == "green")
			$color = '#80ffbf';
		else
			$color = 'default';
		$strHTML = '<div class="'.$this->strIDsPrefix.$strKey.'" style="text-align:center; display:inline; background-color: '.$color.';">'.$object[$strKey].'</div>';
		$strHTML .= "<script>trickyColor('".$this->strIDsPrefix.$strKey."', '".$color."');</script>";

		return $strHTML;
	}

	function WYSIWYGElementEdit($strKey)
	{
		$strHTML = '
		<div id="'.$this->strIDsPrefix.$strKey.'_summernote" style="height:200px;">'.$this->arrObject[$strKey].'</div>
		<textarea style="display:none" type="hidden" id="'.$this->strIDsPrefix.$strKey.'" name="'.$strKey.'">
		'.$this->arrObject[$strKey].'
		</textarea>';
		$strHTML .= "
		<script>
			$('#".$this->strIDsPrefix.$strKey."_summernote').summernote({
				minHeight: 100,
				toolbar: [
					// [groupName, [list of button]]
					['paragraph', ['style']],
					['style', ['bold', 'italic', 'underline', 'clear']],
					['font', ['strikethrough', 'superscript', 'subscript', 'ol', 'ul']],
					['color', ['color']],
					['insert', ['link', 'hr']],
					['misc', ['undo', 'redo']]
				],
				callbacks: {
					onChange: function(contents, ".'$editable'.") {
						document.getElementById('$this->strIDsPrefix$strKey').value = contents;
					}
				}
			});
		</script>";

		return $strHTML;
	}

	function WYSIWYGElementDisplay($object, $strKey)
	{
		return '<div>'.mb_strimwidth(strip_tags($object[$strKey]), 0, 100, "...").'</div>';
	}

	function dateTimeElementDisplay($object, $strKey)
	{
		return $object[$strKey];
	}

	function dateTimeElementEdit($strKey)
	{
		$strHTML = "";
		$strHTML .= '
			<div class="form-group">
				<div class="input-group date" id="'.$this->strIDsPrefix."_control".$strKey.'">
					<input type="text" class="form-control" id="'.$this->strIDsPrefix.$strKey.'" name="'.$strKey.'"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
				</div>
			</div>';
		$strHTML .= "
			<script type='text/javascript'>
				$(function () {
					$('#".$this->strIDsPrefix."_control".$strKey."').datetimepicker({
						defaultDate: new Date(";
		if(!empty($this->arrObject[$strKey]))
			$strHTML .= "'".$this->arrObject[$strKey]."'";
		$strHTML .= "),
						allowInputToggle: true
					});
				});
			</script>";

		return $strHTML;
	}

	function group2selectmultiselectElementEdit($strKey)
	{
		return $this->multiselectElementEdit($strKey);
	}

	function group2selectmultiselectElementDisplay($object, $strKey)
	{
		return $this->multiselectElementDisplay($object, $strKey);
	}

	function renderTabs($arrTitles, $arrValues, $arrContents, $strActiveValue = "", $bDisableOthers = false)
	{
		if(count($arrTitles) != count($arrContents) || count($arrContents) != count($arrValues))
			throw new GeneralException("not equal...");

		$strHTML = "";

		$strHTML .= '<ul class="nav nav-tabs">';

		$bOK = false;
		foreach($arrTitles as $nKey => $strTitle)
		{
			if($arrValues[$nKey] == $strActiveValue || (!$bOK && $strActiveValue == ""))
			{
				$bOK = true;
				$strHTML .= '<li class="active"><a data-toggle="tab" href="#'.$arrValues[$nKey].'">'.$strTitle.'</a></li>';
			}
			else
			{
				if($bDisableOthers)
					$strHTML .= '<li class="disabled"><a href="#">'.$strTitle.'</a></li>';
				else
					$strHTML .= '<li><a data-toggle="tab" href="#'.$arrValues[$nKey].'">'.$strTitle.'</a></li>';
			}
		}

		$strHTML .= '
			</ul>
			
			<div class="tab-content">';

		$bOK = false;

		foreach($arrContents as $nKey => $strContent)
		{
			$strHTML .= '<div id="'.$arrValues[$nKey].'" class="tab-pane fade';
			if($arrValues[$nKey] == $strActiveValue || (!$bOK && $strActiveValue == ""))
			{
				$bOK = true;
				$strHTML .= ' in active';
			}
			$strHTML .= '">';
			$strHTML .= $strContent;
			$strHTML .= '</div>';
		}

		return $strHTML;

	}


	public static function renderElement($strElementName, $arrElementProps, $strElementContent = "")
	{
		$strHTML = "<".$strElementName;
		foreach($arrElementProps as $strKey => $strValue)
		{
			if($strValue != "")
				$strHTML .= " ".$strKey.'="'.$strValue.'"';
		}
		$strHTML .= ">".$strElementContent;
		$strHTML .= "</".$strElementName.">";
		return $strHTML;
	}
}