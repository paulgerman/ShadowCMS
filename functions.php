<?php
//namespace ShadowCMS;

/**
 * @return ShadowCMS\CustomPDO\CustomPDO
 */
function trdb()
{
	return \ShadowCMS\CustomPDO\CustomPDO::singleton();
}

//http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size
function convertPHPSizeToBytes($sSize)
{
	if ( is_numeric( $sSize) )
	{
		return $sSize;
	}
	$sSuffix = substr($sSize, -1);
	$iValue = substr($sSize, 0, -1);
	switch(strtoupper($sSuffix))
	{
		case 'P':
			$iValue *= 1024;
		case 'T':
			$iValue *= 1024;
		case 'G':
			$iValue *= 1024;
		case 'M':
			$iValue *= 1024;
		case 'K':
			$iValue *= 1024;
			break;
	}
	return $iValue;
}

function getMaximumFileUploadSize()
{
	return min(convertPHPSizeToBytes(ini_get('post_max_size')), convertPHPSizeToBytes(ini_get('upload_max_filesize')));
}

function require_once_res($strDirectory, $strFileRelative, $strAlternativeID = "")
{
	static $arrFilesAlreadyIncluded = array();
	$strRelativePath = str_replace(realpath($_SERVER['DOCUMENT_ROOT']), "", realpath($strDirectory));
	$strRelativePath = str_replace("\\", "/", $strRelativePath)."/";
	
	if(!in_array($strRelativePath.$strFileRelative, $arrFilesAlreadyIncluded) && !in_array($strAlternativeID, $arrFilesAlreadyIncluded))
	{
		$arrFilesAlreadyIncluded[] = $strRelativePath.$strFileRelative;
		if($strAlternativeID != "")
			$arrFilesAlreadyIncluded[] = $strAlternativeID;

		if(strtolower(pathinfo($strFileRelative, PATHINFO_EXTENSION)) == "js")
			return '<script src="'.$strRelativePath.$strFileRelative.'"></script>';
		else
			return '<link href="'.$strRelativePath.$strFileRelative.'" rel="stylesheet">';
	}
}



function browser_print($mxValue)
{
	if(is_array(($mxValue)))
		echo "<script>console.log('".javascript_escape(print_r($mxValue, true))."')</script>";
	else
		echo "<script>console.log('".javascript_escape($mxValue)."')</script>";
}

function javascript_escape($str) {
	$new_str = '';

	$str_len = strlen($str);
	for($i = 0; $i < $str_len; $i++) {
		$new_str .= '\\x' . sprintf('%02x', ord(substr($str, $i, 1)));
	}

	return $new_str;
}

function trimHTML($code, $limit = 300)
{
	if ( strlen($code) <= $limit )
	{
		return $code;
	}

	$html = substr($code, 0, $limit);
	preg_match_all ( "#<([a-zA-Z]+)#", $html, $result );

	foreach($result[1] AS $key => $value)
	{
		if ( strtolower($value) == 'br' )
		{
			unset($result[1][$key]);
		}
	}
	$openedtags = $result[1];

	preg_match_all ( "#</([a-zA-Z]+)>#iU", $html, $result );
	$closedtags = $result[1];

	foreach($closedtags AS $key => $value)
	{
		if ( ($k = array_search($value, $openedtags)) === FALSE )
		{
			continue;
		}
		else
		{
			unset($openedtags[$k]);
		}
	}

	if ( empty($openedtags) )
	{
		if ( strpos($code, ' ', $limit) == $limit )
		{
			return $html."...";
		}
		else
		{
			return substr($code, 0, strpos($code, ' ', $limit))."...";
		}
	}

	$position = 0;
	$close_tag = '';
	foreach($openedtags AS $key => $value)
	{
		$p = strpos($code, ('</'.$value.'>'), $limit);

		if ( $p === FALSE )
		{
			$code .= ('</'.$value.'>');
		}
		else if ( $p > $position )
		{
			$close_tag = '</'.$value.'>';
			$position = $p;
		}
	}

	if ( $position == 0 )
	{
		return $code;
	}

	return substr($code, 0, $position).$close_tag."...";
}

function displayQueryCount($strTag)
{
	echo '<script>console.log("#'.$strTag.' queries: '.trdb()->nQueryNum.'");</script>';
}

function is_associative_array($arr)
{
	if(!is_array($arr))
		return FALSE;
	
	foreach (array_keys($arr) as $key => $val)
	{
		if ($key !== $val)
		{
			return TRUE;
		}
	}

	return FALSE;
}

function retrieveObjConfig($strObjectName)
{
	if(!class_exists($strObjectName))
		$strFullObjectName = "\\Objects\\".$strObjectName;
	else
		$strFullObjectName = $strObjectName;

	if(!class_exists($strFullObjectName))
		throw new Exception("Class ". $strFullObjectName ." does not exist");

	$arrProps = array(
		"cols" => $strFullObjectName::$arrRenderProps,
		"enumKeys" => $strFullObjectName::$arrPropsEnum,
		"readOnlyKeys" => $strFullObjectName::$arrPropsReadOnly,
		"indexKey" => $strFullObjectName::$strIndexProp,
		"objectType" => $strObjectName,
		"parentType" => $strObjectName::$parent,
	);
	return $arrProps;
}

function loadObjConfigs($arrObjectNames)
{
	$strHTML = "
	<script>
	var arrRenderConfigs = [];
	var nMaxFileSize = ".getMaximumFileUploadSize().";
	var nMaxImageSize = ".getMaximumFileUploadSize().";";

	foreach($arrObjectNames as $strObjectName)
	{
		if(class_exists($strObjectName))
			$strHTML .= '
				arrRenderConfigs["'.(new \ReflectionClass($strObjectName))->getShortName().'"] = '.json_encode(retrieveObjConfig($strObjectName));
	}

	$strHTML.= "
	</script>";

	foreach ($arrObjectNames as $strObjectName)
	{
		if(class_exists($strObjectName))
			$strClassName = (new \ReflectionClass($strObjectName))->getShortName();
		else
			$strClassName = $strObjectName;

		$strHTML .= '
			<script src="/'.$GLOBALS["relative_path"].'frontEnd/'.$strClassName.'.js?v='.VERSION.'"></script>';
	}
	return $strHTML;
}

function timer($bReset = false)
{
	static $start = 0;
	if(!$bReset)
		$start = microtime(true);
	else
	{
		return ($time_elapsed_secs = (microtime(true) - $start))."sec<br>";
	}
}

/**
 * Groups an array by a given key.
 *
 * Groups an array into arrays by a given key, or set of keys, shared between all array members.
 *
 * Based on {@author Jake Zatecky}'s {@link https://github.com/jakezatecky/array_group_by array_group_by()} function.
 * This variant allows $key to be closures.
 *
 * @param array $array   The array to have grouping performed on.
 * @param mixed $key,... The key to group or split by. Can be a _string_,
 *                       an _integer_, a _float_, or a _callable_.
 *
 *                       If the key is a callback, it must return
 *                       a valid key from the array.
 *
 *                       ```
 *                       string|int callback ( mixed $item )
 *                       ```
 *
 * @return array|null Returns a multidimensional array or `null` if `$key` is invalid.
 */

function array_group_by( array $array, $key )
{
	if ( ! is_string( $key ) && ! is_int( $key ) && ! is_float( $key ) && ! is_callable( $key ) ) {
		trigger_error( 'array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR );
		return null;
	}

	$func = ( is_callable( $key ) ? $key : null );
	$_key = $key;

	// Load the new array, splitting by the target key
	$grouped = [];
	foreach ( $array as $value ) {
		if ( is_callable( $func ) ) {
			$key = call_user_func( $func, $value );
		} elseif ( is_object( $value ) && isset( $value->{ $_key } ) ) {
			$key = $value->{ $_key };
		} elseif ( isset( $value[ $_key ] ) ) {
			$key = $value[ $_key ];
		} else {
			continue;
		}

		$grouped[ $key ][] = $value;
	}

	// Recursively build a nested grouping if more parameters are supplied
	// Each grouped array value is grouped according to the next sequential key
	if ( func_num_args() > 2 ) {
		$args = func_get_args();

		foreach ( $grouped as $key => $value ) {
			$params = array_merge( [ $value ], array_slice( $args, 2, func_num_args() ) );
			$grouped[ $key ] = call_user_func_array( 'array_group_by', $params );
		}
	}

	return $grouped;
}


function debug_log($object = null, $label = null)
{
	$message = json_encode($object, JSON_PRETTY_PRINT);
	$label = "Debug".($label ? " ($label): " : ': ');
	echo "<script>console.log(\"$label\", $message);</script>";
}

function getBlacklist()
{
	return @file_get_contents("blacklist");
}

function buildURL($arrExcept = [])
{
	if(!is_array($arrExcept))
		$arrExcept = [$arrExcept];

	$strURL = '?';
	foreach($_GET as $key => $value)
	{
		if(!in_array($key, $arrExcept) && $key != "search")
			if(is_array($value))
				foreach($value as $val)
					$strURL .= "&".$key."[]=".$val;
			else
				$strURL .= "&".$key."=".$value;
	}
	return $strURL;
}