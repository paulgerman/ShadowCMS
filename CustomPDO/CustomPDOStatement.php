<?php
namespace ShadowCMS\CustomPDO;
use ShadowCMS\GeneralException;

class CustomPDOStatement extends \PDOStatement
{
	/*
	 * @var CustomPDO $pdo
	 */
	protected $pdo;

	protected function __construct($pdo)
	{
		$this->pdo = $pdo;
	}


	public function execute($args = null)
	{
		// Perform logging here. PDO object is accessible
		// from $this->pdo.

		$bSuccess = parent::execute($args);
		if($bSuccess === false)
		{
			$arrErrorInfo = $this->errorInfo();
			throw new GeneralException("Mysql error: ".print_r($arrErrorInfo, true)." ".$this->queryString);

		}
		return $bSuccess;
	}

}