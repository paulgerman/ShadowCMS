<?php
namespace ShadowCMS\CustomPDO;
use ShadowCMS\GeneralException;

include "CustomPDOStatement.php";
class CustomPDO extends \PDO
{
	protected static $singleton = null;
	protected static $arrConfig = null;

	private $bLogging = false;
	private $strLogs = "";
	private $nLogThreshold = 0;

	public $nQueryNum = 0;
	public $nTotalExecutionTime = 0;

	public static function setConfig($arrConfig)
	{
		static::$arrConfig = $arrConfig;
	}

	public function __construct($arrConfig)
	{
		// turn of error reporting
		mysqli_report(MYSQLI_REPORT_OFF);

		// connect to database
		parent::__construct(
			"mysql:host=".$arrConfig["host"]."; port=".$arrConfig["port"]."; dbname=".$arrConfig["DB"]."; charset=utf8mb4",
			$arrConfig["username"],
			$arrConfig["password"],
			[\PDO::MYSQL_ATTR_INIT_COMMAND =>"SET time_zone = '+00:00'"]
		);
		$this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array(\ShadowCMS\CustomPDO\CustomPDOStatement::class, array($this)));


		// check if a connection established
		if(mysqli_connect_errno())
		{
			throw new \Exception(mysqli_connect_error(), mysqli_connect_errno());
		}
	}

	public static function singleton()
	{
		if(!self::$singleton)
		{
			self::$singleton = new self(static::$arrConfig);
		}
		return self::$singleton;
	}

	public function query($strQuery)
	{
		if($this->bLogging)
			$timeStart = microtime(true);

		$statement = parent::query($strQuery);
		if($this->errorCode() != '00000')
		{
			$arrErrorInfo = $this->errorInfo();
			throw new GeneralException($arrErrorInfo[0].": ".$arrErrorInfo[2]." ".$strQuery);
		}

		if($this->bLogging)
		{
			$this->nQueryNum++;
			$timeElapsed = microtime(true) - $timeStart;
			if($timeElapsed >= $this->nLogThreshold)
				$this->strLogs .= "$timeElapsed: $strQuery\n\n";
			$this->nTotalExecutionTime += $timeElapsed;
		}
		return $statement;
	}

	public function exec($strQuery)
	{
		if($this->bLogging)
			$timeStart = microtime(true);

		$nAffectedRows = parent::exec($strQuery);

		if($this->errorCode() != '00000')
		{
			$arrErrorInfo = $this->errorInfo();
			throw new GeneralException("SQL: ".$arrErrorInfo[0].": ".$arrErrorInfo[2]." ".$strQuery);
		}

		if($this->bLogging)
		{
			$this->nQueryNum++;
			$timeElapsed = microtime(true) - $timeStart;
			if($timeElapsed >= $this->nLogThreshold)
				$this->strLogs .= "$timeElapsed: $strQuery\n\n";

			$this->nTotalExecutionTime += $timeElapsed;
		}

		return $nAffectedRows;
	}

	public function enableLogging($nLogThreshold = 0)
	{
		$this->bLogging = true;
		$this->nLogThreshold = $nLogThreshold;
	}

	public function dumpLogs()
	{
		return $this->strLogs;
	}
}