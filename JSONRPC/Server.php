<?php

namespace ShadowCMS\JSONRPC;


use ShadowCMS\JSONRPC\Plugins\PluginBase;
use ShadowCMS\GeneralException;


abstract class Server
{
	/**
	 * @var Plugins\PluginBase[]
	 */
	protected $plugins = array();

	protected $arrAllowedFunctions = array();

	/**
	 * Return an associative array of strings that represent the allowed function calls.
	 * To allow a class with all functions: "user" => "\\Controllers\\UserController",
	 * To allow only certain functions from a class: "user_get" => "\\Controllers\\UserController()->get"
	 * To allow a function: "destroy_everything_alive" => "destroy_everything_alive"
	 * @return array
	 */
	abstract function allowedFunctions();

	public function __construct($strLogPath)
	{
		$this->strLogPath = $strLogPath.(new \ReflectionClass($this))->getShortName()."/";
		if(!is_dir($this->strLogPath))
			@mkdir($this->strLogPath, 0777, true);

		$this->arrAllowedFunctions = $this->allowedFunctions();
		$this->addPlugins();
	}

	public function addPlugins()
	{

	}

	public function addPlugin(PluginBase $plugin)
	{
		$this->plugins[] = $plugin;
	}

	final function processRequestBatch($arrData)
	{
		if(is_array($arrData))
			if(is_associative_array($arrData))
				return $this->processRequest($arrData);
			else
			{
				$arrAnswers = array();
				foreach($arrData as $arrRequest)
					$arrAnswers[] = $this->processRequest($arrRequest);
				return '['.join(",", $arrAnswers)."]";
			}
		else
			throw new JSONRPCException("Data must be array");
	}

	final function processRequest($arrData)
	{
		try
		{
			$pdo = trdb();
		}
		catch(\PDOException $exc)
		{
			throw new JSONRPCException($exc->getMessage());
		}

		if(!is_array($arrData))
		{
			throw new JSONRPCException("Data must be array");
		}
		if(!array_key_exists("method", $arrData))
		{
			throw new JSONRPCException("'method' key does not exist in array");
		}
		if(!array_key_exists("params", $arrData))
		{
			throw new JSONRPCException("'params' key does not exist in array");
		}

		if(is_object($arrData["params"]))
		{
			$arrData["params"] = (array)$arrData["params"];
		}

		if(!is_array($arrData["params"]))
		{
			throw new JSONRPCException("'params' key must be an array");
		}

		foreach($this->plugins as $plugin)
		{
			$arrData = $plugin->beforeProcess($arrData);
		}

		if(array_key_exists($arrData["method"], $this->arrAllowedFunctions))
		{
			if(
				$this->arrAllowedFunctions[$arrData["method"]] === $arrData["method"]
				|| (
					is_array($this->arrAllowedFunctions[$arrData["method"]])
					&& count($this->arrAllowedFunctions[$arrData["method"]]) === 1
					&& $this->arrAllowedFunctions[$arrData["method"]][0] === $arrData["method"]
				)
			)
			{
				if(function_exists($arrData["method"]))
				{
					$reflectionMethod = new \ReflectionFunction($arrData["method"]);
					$this->_checkParams($arrData["params"], $reflectionMethod);

					try
					{
						$pdo->beginTransaction();
						$arrResult = call_user_func_array($arrData["method"], $arrData["params"]);
						$pdo->commit();
					}
					catch(GeneralException $exc)
					{
						return $this->_logError($exc);
					}
				}
				else
				{
					throw new JSONRPCException("Function ".$arrData["method"]." is not defined");
				}
			}
			else
			{
				if(is_array($this->arrAllowedFunctions[$arrData["method"]]))
				{
					$strClassName = $this->arrAllowedFunctions[$arrData["method"]][0];
					$strFunctionName = @$this->arrAllowedFunctions[$arrData["method"]][1];
				}
				else
				{
					$strCallInfo = $this->arrAllowedFunctions[$arrData["method"]];
					$strClassName = substr($strCallInfo, 0, strpos($strCallInfo, "()->"));
					$strFunctionName = substr($strCallInfo, strpos($strCallInfo, "()->") + 4);
				}
				if(empty($strFunctionName))
				{
					throw new JSONRPCException("Method from class ".$strClassName." not specified");
				}

				try
				{
					$arrResult = $this->_callClassMethod($strClassName, $strFunctionName, $arrData["params"]);
				}
				catch(GeneralException $exc)
				{
					return $this->_logError($exc);
				}
			}
		}
		else if(array_key_exists($strClassNameRequested = substr($arrData["method"], 0, strpos($arrData["method"], "_")), $this->arrAllowedFunctions))
		{
			if(is_array($this->arrAllowedFunctions[$strClassNameRequested]))
			{
				$strClassName = $this->arrAllowedFunctions[$strClassNameRequested][0];
				$strFunctionName = substr($arrData["method"], strpos($arrData["method"], "_") + 1);
			}
			else
			{
				$strClassName = $this->arrAllowedFunctions[$strClassNameRequested];
				$strFunctionName = substr($arrData["method"], strpos($arrData["method"], "_") + 1);
			}
			try
			{
				$arrResult = $this->_callClassMethod($strClassName, $strFunctionName, $arrData["params"]);
			}
			catch(GeneralException $exc)
			{
				return $this->_logError($exc);
			}
		}
		else
		{
			throw new JSONRPCException("Function ".$arrData["method"]." is not allowed on this endpoint!");
		}

		foreach($this->plugins as $plugin)
		{
			$arrResult = $plugin->afterProcess($arrResult);
		}

		$strEncodedResult =  json_encode(array(
			"jsonrpc" => "2.0",
			"result" => $arrResult,
		));
		if(json_last_error() !== JSON_ERROR_NONE)
			throw new JSONRPCException("JSON encode error: ".json_last_error()." ".(function_exists("json_last_error_msg")?json_last_error_msg():" "));

		return $strEncodedResult;
	}

	private function _callClassMethod($strClassName, $strFunctionName, $arrParams)
	{
		$pdo=trdb();
		if(class_exists($strClassName))
		{
			$reflection = new \ReflectionClass($strClassName);
			$arrMethods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);
			foreach($arrMethods as $method)
			{
				if($method->getName() == $strFunctionName)
				{
					$objController = new $strClassName();

					$reflectionMethod = new \ReflectionMethod($strClassName, $strFunctionName);
					$this->_checkParams($arrParams, $reflectionMethod);

					$pdo->beginTransaction();
					$arrResult = call_user_func_array(array(&$objController, $strFunctionName), $arrParams);
					$pdo->commit();
					return $arrResult;

				}
			}
			throw new JSONRPCException("Method ".$strFunctionName." does not exist in class ".$strClassName);
		}
		else
		{
			throw new JSONRPCException("Class ".$strClassName." does not exist!");
		}
	}

	private function _checkParams($arrParams, \ReflectionFunctionAbstract $reflectionFunction)
	{
		if($this->_arrayIsAssoc($arrParams))
		{
			//TODO
			throw new JSONRPCException("Associative array as parameters of function call is not yet allowed!");
		}
		else
		{
			if(count($arrParams) < $reflectionFunction->getNumberOfRequiredParameters() || count($arrParams) > $reflectionFunction->getNumberOfParameters())
			{
				$arrRequiredParams = array();
				$arrOptionalParams = array();

				$arrReflectionParams = $reflectionFunction->getParameters();
				foreach($arrReflectionParams as $reflectionParameter)
				{
					if(!$reflectionParameter->isOptional())
					{
						$arrRequiredParams[] = $reflectionParameter->getName();
					}
					else
					{
						$arrOptionalParams[] = $reflectionParameter->getName()." = ".$reflectionParameter->getDefaultValue();
					}
				}
				if(count($arrParams) < $reflectionFunction->getNumberOfRequiredParameters())
				{
					throw new JSONRPCException("Too few parameters. Required params: ".implode(", ", $arrRequiredParams)." Optional params: ".implode(", ", $arrOptionalParams));
				}
				else if(count($arrParams) > $reflectionFunction->getNumberOfParameters())
				{
					throw new JSONRPCException("Too many parameters. Required params: ".implode(", ", $arrRequiredParams)." Optional params: ".implode(", ", $arrOptionalParams));
				}
			}
		}
	}

	private function _arrayIsAssoc(array $array) {
		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}

	private function _logError(\Exception $exc)
	{
		$pdo=trdb();
		if($pdo->inTransaction())
			$pdo->rollBack();
		error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".$exc->getFile()."#".$exc->getLine()."\n".$exc->getMessage()."\n".$exc->getTraceAsString()."\n\n",3,$this->strLogPath."apiErrors.log");
		return json_encode(array(
			"jsonrpc" => "2.0",
			"error" => array(
				"code" => $exc->getCode(),
				"message" => $exc->getMessage(),
			),
		));
	}

	public function start()
	{
		$time_start = microtime(true);

		if(count($_POST))
		{
			if(isset($_POST["q"]))
			{
				$strJSONEncodedData = $_POST["q"];
			}
			else
			{
				exit("Nothing requested POST :( q parameter is missing");
			}
		}
		else if(count($_GET))
		{
			if(isset($_GET["q"]))
			{
				$strJSONEncodedData = urldecode($_GET["q"]);
			}
			else
			{
				exit("Nothing requested GET :( q parameter is missing");
			}
		}
		else
		{
			$strJSONEncodedData = file_get_contents('php://input');
			if($strJSONEncodedData == "")
			{
				?>
				<form method="post">
					<textarea name="q" style="width: 100%;height: 50%">{"method":"method_name","params":[]}</textarea>
					<input type="hidden" name="pretty" value="1">
					<input type="submit" value="SUBMIT">
				</form>
				<?php
				exit("Nothing requested :(");
			}
		}
		error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".$strJSONEncodedData."\n\n",3,$this->strLogPath."jsonRequests.log");

		$arrData = json_decode($strJSONEncodedData, true);
		try
		{
			if ($nError = json_last_error() !== JSON_ERROR_NONE)
			{
				throw new JSONRPCException("Invalid json. Error: ".$nError.' '.json_last_error_msg());
			}
			$arrData = (array)$arrData;
			error_log(gmdate("Y-m-d\\TH:i:s\\Z")." - ".$_SERVER["REMOTE_ADDR"].": ".var_export($arrData,true)."\n\n",3,$this->strLogPath."requests.log");

			$arrAnswer = $this->processRequestBatch($arrData);
			if(array_key_exists("pretty", $_REQUEST))
			{
				$execution_time = (microtime(true) - $time_start);
				echo '<b>Total execution time:</b> '.$execution_time.' sec<br>';
				echo 'Peak memory usage: '.(memory_get_peak_usage(true)/1024/1024)."MB";
				echo '<textarea style="width: 100%; height: 100%;">'.json_encode(json_decode($arrAnswer), JSON_PRETTY_PRINT).'</textarea>';
			}
			else
			{
				header('Content-type: application/json');
				echo $arrAnswer;
			}
		}
		catch(JSONRPCException $exc)
		{
			echo $this->_logError($exc);
		}
	}
}