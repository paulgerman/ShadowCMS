<?php
namespace ShadowCMS\JSONRPC\Plugins;

use ShadowCMS\JSONRPC\JSONRPCException;

class FilterPlugin implements PluginBase
{
	private $arrForbiddenKeys;
	public function __construct(Array $arrForbiddenKeys)
	{
		$this->arrForbiddenKeys = $arrForbiddenKeys;
	}

	function beforeProcess($arrData)
	{
		return $arrData;
	}

	function afterProcess($arrData)
	{
		return $this->removeData($arrData);
	}

	private function removeData($arrData)
	{
		if(is_array($arrData))
			foreach($arrData as $key => $value)
			{
				if(!empty($key) && in_array($key, $this->arrForbiddenKeys))
				{
					unset($arrData[$key]);
				}
				elseif(is_associative_array($value))
				{
					$arrData[$key] = $this->removeData($value);
				}
			}
		return $arrData;
	}
}