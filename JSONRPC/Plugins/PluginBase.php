<?php

namespace ShadowCMS\JSONRPC\Plugins;


interface PluginBase
{
	public function beforeProcess($arrData);

	public function afterProcess($arrData);
}