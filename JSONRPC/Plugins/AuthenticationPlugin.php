<?php
namespace ShadowCMS\JSONRPC\Plugins;


use ShadowCMS\JSONRPC\JSONRPCException;

class AuthenticationPlugin implements PluginBase
{
	private $fnValidate;
	private $strCookieName;
	public function __construct(callable $fnValidate = null, $strCookieName = "auth")
	{
		$this->fnValidate = $fnValidate;
		$this->strCookieName = $strCookieName;
	}

	function beforeProcess($arrData)
	{
		$bAuthenticated = false;
		if(array_key_exists($this->strCookieName, $_COOKIE))
		{
			if($this->fnValidate !== null)
			{
				$fn = $this->fnValidate;
				$bAuthenticated = $fn($_COOKIE[$this->strCookieName]);
			}
			else
				$bAuthenticated = $this->_authTest($_COOKIE[$this->strCookieName]);
		}
		else
		{
			throw new JSONRPCException("To use this endpoint you need to use authentication!");
		}

		if(!$bAuthenticated)
		{
			throw new JSONRPCException("Authentication failed!");
		}

		return $arrData;
	}

	function afterProcess($arrData)
	{
		return $arrData;
	}


	private function _authTest($strToken)
	{
		if($strToken == "1235")
			return true;
		return false;
	}

	private function _authFacemFacem($strToken)
	{
		$pdo = trdb();

		if(strlen($strToken))
		{
			$strId = $pdo->query("
				SELECT `userid` FROM
				 `session`
				WHERE
				 AND `sessionhash` = ".$pdo->quote($strToken)."
			")->fetch(\PDO::FETCH_COLUMN);

			if($strId !== false)
			{
				$arrData["params"][0] = (int)$strId;
				return true;
			}

			return false;
		}
	}
}