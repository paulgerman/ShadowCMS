<?php

namespace ShadowCMS\Controllers;
use ShadowCMS\Objects\ChangeLog;

class ChangeLogController extends \ShadowCMS\BaseController
{
	function __construct()
	{
		$this->strClassName = ChangeLog::class;
	}

	function add($strRefName, $nRefID, $strType, $nUserID = NULL)
	{
		$arrData = [];
		$arrData["change_log_ip"] = $_SERVER["REMOTE_ADDR"];
		$arrData["change_log_ref_name"] = $strRefName;
		$arrData["change_log_ref_id"] = $nRefID;
		$arrData["change_log_type"] = $strType;
		$arrData["user_id"] = $nUserID;
		return $this->create($arrData);
	}
}